# frozen_string_literal: true

require 'omniauth-oauth2'
require 'omniauth-alicloud/version'
require 'omniauth/strategies/alicloud'
