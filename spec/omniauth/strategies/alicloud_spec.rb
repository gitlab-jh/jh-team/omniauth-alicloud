# frozen_string_literal: true
require 'spec_helper'

RSpec.describe OmniAuth::Strategies::Alicloud do
  let(:client) { OAuth2::Client.new('client_id', 'client_secret') }
  let(:app) { -> { [200, {}, ['Hello.']] } }
  let(:request) { double('Request', params: {}, cookies: {}, env: {}) }

  subject do
    OmniAuth::Strategies::Alicloud.new(app, 'client_id', 'client_secret', @options || {}).tap do |strategy|
      allow(strategy).to receive(:request) {
        request
      }
    end
  end

  before do
    OmniAuth.config.test_mode = true
  end

  after do
    OmniAuth.config.test_mode = false
  end

  context '#client options' do
    it 'should have correct name' do
      expect(subject.options.name).to eq('alicloud')
    end

    it 'should have correct site' do
      expect(subject.options.client_options.site).to eq('https://oauth.aliyun.com/')
    end
  end

  describe 'callback_url' do
    let(:base_url) { 'https://example.com' }

    it 'has the correct default callback path' do
      allow(subject).to receive(:full_host) { base_url }
      allow(subject).to receive(:script_name) { '' }
      allow(subject).to receive(:query_string) { '' }
      expect(subject.callback_url).to eq(base_url + '/auth/alicloud/callback')
    end

    it 'should set the callback path with script_name if present' do
      allow(subject).to receive(:full_host) { base_url }
      allow(subject).to receive(:script_name) { '/v1' }
      allow(subject).to receive(:query_string) { '' }
      expect(subject.callback_url).to eq(base_url + '/v1/auth/alicloud/callback')
    end
  end
end
