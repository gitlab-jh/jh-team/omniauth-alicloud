# frozen_string_literal: true

RSpec.describe OmniAuth::Alicloud do
  it "has a version number" do
    expect(OmniAuth::Alicloud::VERSION).not_to be nil
  end
end
