# Change Log

## v2.0.0 (2022-09-16)

Makes compatible with OmniAuth 2 and requires it.

Note: https://gitlab.com/gitlab-jh/jh-team/omniauth-alicloud/-/merge_requests/1 for reasoning - Thanks @jessieay

_Major version bump as no longer supports Omniauth 1._

## v2.0.1 (2023-01-17)

Fix callback url mismatch.

## v3.0.0 (2023-09-15)

Change use sub openid as omniauth uid.